---

marp: true
theme: uncover
paginate: true
backgroundColor: #fff
color: #595959
colorSecondary: ##009999
backgroundImage: url('images/deafult_background.png')
style: |
    section{
      font-family: "Helvetica", monospace;
    }
    h2 {
    font-size: 50pt;
    list-style-type: circle;
    font-weight: 900;
    color: #009999;
    text-align: center;
    }
    section::after {
      font-weight: bold;
      content: attr(data-marpit-pagination);
      font-size: 13pt;
      color: #595959;
    }    
---

<style scoped>
  h2 {
    font-size: 40pt;
    list-style-type: circle;
    font-weight: 900;
    color: #fff
  }
  p {
    font-size: 23pt;
    list-style-type: circle;
    font-weight: 500;
    color: #fff
  }
</style>

<!-- _backgroundImage: "url('images/capa.png')" -->
<!-- _paginate: false -->

---

<!-- _paginate: false -->
> ## *Criatividade é pensar coisas novas. Inovação é fazê-las.*

*Theodore Levitt*

---
<style scoped>
  h4 {
    font-size: 35pt;
    list-style-type: circle;
    font-weight: 900;
    color: #009999
  }
  p {
    font-size: 13pt;
  }
  {
   font-size: 32px;
  }
</style>

![bg right:32% 80%](images/perfil.jpeg)

#### 🇧🇷 Samuel Gonçalves Pereira

* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência 💻

---

<style scoped>
  p {
  font-size: 19pt;
  list-style-type: circle;
}
</style>
![bg left:40% 80%](images/qrcode-slides.png)
### Esses slides são OpenSource!
É só escanear ou acessar o link:
[gitlab.com/o_sgoncalves/software-livre-nas-empresas](https://gitlab.com/o_sgoncalves/software-livre-nas-empresas)

---

<style scoped>
  h3 {
    font-size: 40pt;
    list-style-type: circle;
    font-weight: 900;
    color: #009999
  }
  p {
    font-size: 20pt;
  }
  {
   font-size: 35px;
  }
</style>

### SORTEIO NO FIM DA PALESTRA!
![bg right:50% 60%](images/qrcode-sorteio.png)
##### Inscreva-se e participe!
Escaneie o QRCode ou [clique aqui!](https://sorteio.4linux.com.br/sorteio-palestra-aumente-a-seguranca-de-suas-aplicacoes-com-software-livre)

---

<!-- _backgroundImage: "url('images/background-hacker.png')" -->
<!-- _paginate: false -->
# **Precisamos** falar de
## Software Livre!

---

<style scoped>
  h2 {
    font-size: 50pt;
    list-style-type: circle;
    font-weight: 900;
    color: #009999;
    text-align: center;
  }
  p {
    font-size: 22pt;
    text-align: left;
  }
  {
   font-size: 25px;
   text-align: left;
  }
</style>

## Software Livre


* **Liberdade 0** - Liberdade para o programa para quaisquer propósitos;
* **Liberdade 1** - Liberdade para estudar como o programa trabalha e adaptá-lo às suas necessidades. Ter acesso ao código fonte é essencial para isso.
* **Liberdade 2** - Liberdade de redistribuir cópias de forma que você possa ajudar outras pessoas.
* **Liberdade 3** - Liberdade para melhorar o programa e disponibilizar as melhorias para o público, de forma que toda a comunidade possa se beneficiar disso. Ter acesso ao código fonte é essencial também para isso.

---
<style scoped>
  h6 {
    font-size: 15pt;
    font-weight: 500;
    color: #595959;
    text-align: right;
  }
</style>

## Oba! Então é de graça!

**Software livre** é uma questão de ***liberdade***, não de preço. Para entender o conceito, pense em *“liberdade de expressão”*, não em *“cerveja grátis”*. 
#
#

###### Saiba mais: [https://www.gnu.org/philosophy/free-sw.pt-br.html](https://www.gnu.org/philosophy/free-sw.pt-br.html)

---

## Metodologias Ágeis

Tudo começou com a criação do **Agile Manifest** em 2001, onde definiram-se pilares para que a entrega de software fosse otimizada. Assim surgiram as metodologias ágeis.

---

## Grandes mudanças
Com a assensão das metodologias ágeis percebeu-se a importância de uso de tecnologias cada vez mais automatizadas e compatíveis com os requisitos atuais de criação de software

---

## Novos desafios da Tecnologia
* A infraestrutura *on premisses* vem sendo substituída por *computação em nuvem*
* Clusters de máquinas virtuais vem sendo substituídos por clusters em containers usando kubernetes
* Fala-se em low code ou até mesmo em no code


---

## Novas habilidades
* Alguns cargos em tecnologia vem sendo atualizados!
* Sysadmin virou DevOps Engineer ou SRE
* A preocupação com agilidade tornou-se padrão em times de tecnologia

---

## O que é DevOps?

É uma abordagem de cultura, automação e design de plataforma que tem como objetivo agregar mais valor aos negócios e aumentar sua capacidade de resposta às mudanças por meio de entregas de serviços rápidas e de alta qualidade. 

---
<style scoped>
  li {
    font-size: 25pt;
    font-weight: 500;
    list-style-type:square;
    color: #595959;
    text-align: left;
    align: left;
  }

</style>

## Pilares do DevOps

* **Culture**
* **Automation**
* **Lean It**
* **Measurement**
* **Sharing**

---

<style scoped>
  h2 {
    font-size: 35pt;
    font-weight: 900;
    color: #fff;
    list-style-type:square;
  }
</style>
<!-- _backgroundImage: "url('images/destaque_background.png')" -->
<!-- _paginate: false -->

## Os caminhos da mudança!
#
#
#
#
#
#
#


---

## Provisionamento e Automação com Ansible, AWX e Rundeck

---

## Infraestrutura como Código

É o gerenciamento e provisionamento da infraestrutura por meio de códigos, em vez de processos manuais. Com a IaC, são criados arquivos de configuração que incluem as especificações da sua infraestrutura, facilitando a edição e a distribuição de configurações.

---

## Ansible
Ferramenta OpenSource de automação capaz de configurar sistemas, implantar aplicativos e providenciar orquestração e gerenciamento de tarefas, mantida pela RedHat. Tem suporte a vários sistemas operacionais e plataformas.

---

## AWX
Plataforma opensource para criar e gerenciar jobs de orquestração e implantação. Fornece uma interface web para o Ansible. É possível criar, monitorar e gerenciar os jobs, bem como ver os resultados de implantação.

---

## Rundeck
Ferramenta opensource para gerenciamento de tarefas com shellscript, python, ansible, e diversas outras linguagens. Utilizado para automatizar as mais diversas tarefas de infraestrutura.

---

## Terraform
Ferramenta para construir, alterar e gerenciar infraestrutura em nuvem. É capaz de gerenciar provedores de serviços existentes e populares, bem como soluções internas personalizadas.

---

## Exemplo de IaC

---

## Pipeline de Integração e Entrega Continua - CI/CD – com Jenkins

---

## Jenkins
Servidor opensource usado para automatizar taregas de compilação, teste e implantação de software.

---

## Exemplo de Pipeline

---

## Orquestração de Containers com Kubernetes, Openshift e Rancher

---

## Docker

Software opensource que automatiza o deploy de aplicações dentro de contêineres de software, proporcionando uma forma mais rápida e flexível de implantar aplicativos.

---

## Kubernetes
Sistema de gerenciamento de contêineres opensource, criado pelo Google. Automatiza o acompanhamento, o gerenciamento e o escalonamento de aplicativos em contêineres, permitindo que os desenvolvedores se concentrem apenas nas suas aplicações.

---

## Openshift
Plataforma de aplicações em nuvem opensource mantida pela Red Hat. Oferece um ambiente de nuvem para criar, implantar e gerenciar aplicativos em container, usando 'por baixo do capô' o kubernetes.

---

## Rancher
Plataforma opensource de gerenciamento de containers unificando gerenciamento de clusters kubernetes, docker e mesos. Atualmente mantido pela Suse Inc.

---

## Vamos monitorar?

---

## Zabbix
Ferramenta opensource de monitoramento de infraestrutura. Suporta vários bancos de dados, plataformas e sistemas operacionais. Envia alertas, oferece mapas e dashboards.

---

## Prometheus
Sistema de monitoramento e alertas para aplicações e infraestruturas. Ele foi originalmente criado pelo SoundCloud e liberado como software de código aberto em 2012.

---

## Graylog
Plataforma de gerenciamento de log opensource. Fornece uma interface da Web para pesquisar e visualizar dados de log, podendo ser usado para coletar e monitorar logs do sistema, aplicativos e dados de tráfego de rede.

---
<style scoped>
  li {
    font-size: 25pt;
    font-weight: 500;
    list-style-type:square;
    color: #595959;
    text-align: left;
    align: left;
  }
  p {
    font-size: 25pt;
  }
</style>

## ELK
Stack capaz de coletar e analisar dados de log, composta por:

* **Elasticsearch**: Mecanismo distribuído de pesquisa e análise.
* **Logstash**: Pipeline de processamento de dados para coletar, analisar e enriquecer dados de log.
* **Kibana**: Interface web para visualizar e analisar dados.

---

## Grafana
Ferramenta de visualização de dados opensource. Ele permite que você analise e visualize os dados de diversas fontes de dados de forma unificada.

---

## Exemplo de monitoramento com opensource!

---

## Precisamos falar de Segurança!

---

## DevSecOps
Representa uma evolução natural e necessária na forma como as organizações de desenvolvimento abordam a segurança.

---

No passado, a segurança era 'acrescentada' ao software no final do ciclo de desenvolvimento _(quase como uma reflexão tardia)_ por uma equipe de segurança separada e testada por uma equipe de garantia de qualidade (QA) também separada

---
<style scoped>
  li {
    font-size: 25pt;
    font-weight: 500;
    list-style-type:square;
    color: #595959;
    text-align: left;
    align: left;
  }
  p {
    font-size: 25pt;
  }
</style>

## Benefícios do DevSecOps

* Entrega de software rápida e com boa relação custo-benefício
* Segurança aprimorada e proativa
* Correção de vulnerabilidade de segurança acelerada
* Automação compatível com o desenvolvimento moderno
* Um processo repetível e adaptativo

---

## O que proteger?
![w:700px](images/4cs.png)

---

![bg center](images/DevSecOps-SAST-Pipeline.png)

---

## SAST
Acrônimo para **Teste Estático de Segurança de Aplicação**, analisa o código fonte dos sistemas. Os testes  são realizados **antes** que o sistema esteja em produção.

---

## DAST

**Teste Dinâmico de Segurança de Aplicação** testa as interfaces expostas em busca de vulnerabilidades, após a aplicação estar em execução.

---

<style scoped>
  li {
    font-size: 25pt;
    font-weight: 500;
    list-style-type:square;
    color: #595959;
    text-align: left;
    align: left;
  }
  p {
    font-size: 25pt;
  }
</style>

## Cloud

Para proteger a camada da **cloud** é importante se atentar pelo menos aos pontos:

- IaC Versionada
- Controles de Acesso
- Hardening de instâncias
- Fluxo de acesso à aplicação
- Regras de ﬁrewall
- Alta disponibilidade

---

## SIEM
Ferramenta de monitoramento e análise de segurança de rede que combina ferramentas de monitoramento de rede, detecção de intrusão e análise de log para fornecer visibilidade completa e insights em tempo real sobre ameaças à segurança da empresa.

---

## Exemplo prático de Pipeline DevSecOps

---

## Dúvidas?

---

<style scoped>
p {
  font-size: 25pt;
}
{
 font-size: 25px;
}
a {
  color: #595959
}
</style>

## Obrigado!

Vamos nos conectar?
* **Site:** [sgoncalves.tec.br](https://sgoncalves.tec.br)
* **E-mail:** [samuel@sgoncalves.tec.br](https://sgoncalves.tec.br/contato)
* **Linkedin:** [linkedin.com/in/samuelgoncalvespereira/](linkedin.com/in/samuelgoncalvespereira/)
* **Telegram:** [t.me/Samuel_gp](t.me/Samuel_gp)
* **Todas as redes:** [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

---
<!-- _backgroundImage: "url('images/end.png')" -->
<!-- paginate: false -->